###关于作者
***

**人生苦短，我用Python**

半路出家程序员(猿)，现居于鹭岛，爱好：美食、游戏、乃木坂46。

曾经被JAVA伤害很深，现已投入Python怀抱。

###关于站点 Display Shelf
***

####介绍

站点用于个人项目范例展示。

站点是在个人兴趣指引下用业余时间写的项目，技术还欠缺火候。使用中有问题或建议，欢迎提issue或用以下联系方式跟我交流：

* 邮件: zwtzjd@gmail.com
* QQ: 3084582097
* Github: [taogeT](https://github.com/taogeT)

####当前项目

* 直播站点爬虫数据采集 ([Livetv Mining](http://www.zhengwentao.com/crawler))

    斗鱼 虎牙 战旗 熊猫

* 微信公众号接口研究(深入研究需企业级订阅号支持，目前中止)

* 模拟淘宝购物车(建设中)
